'''
*Name:find_net_amount.py
*Description: To find the net amount in the account after the deposits
*Input:A series of trasaction
*Output:The final amount in the account
'''
import datetime
import re
class Account:
    def __init__(self):
        self.amount = 0
        self.creation_date= datetime.datetime.now()
    
    def deposit (self,amount):
        self.amount +=amount
    
    def withdraw(self,amount):
        self.amount-=amount
    
    def return_balance(self):
        return self.amount

account  = Account()

user_input = input('Enter series of deposit and withdrawal').split(' ')

for request in user_input:
    type_operation = re.findall(r'[DW]{1}',request)
    if(len(type_operation) is 0):
        print('Invalid Format')
        exit()
    type_operation = type_operation[0]
    amount = re.findall(r'[0-9]+',request)
    if(len(amount) is  0):
        print('Invalid amount')
        exit()
    amount = amount[0]

    if type_operation=='D':
        account.deposit(int(amount))
    else :
        account.withdraw(int(amount))

print(account.return_balance())


