class A:
    class_variable = 'I am shared'

    def __init__(self):
        self.instance_variable = 'I am private'
    
    def get_variable(self):
        return  self.instance_variable

a = A()
print(a.class_variable)
print( a.get_variable())
b = A()
print(b.class_variable)
print( a.get_variable())
