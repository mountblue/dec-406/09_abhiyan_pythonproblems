'''
*Name:validate_passwords.py
*Description: To validate the password
*Input:A password
*Output:The validity of the password

At least 1 letter between [a-z]
At least 1 number between [0-9]
At least 1 letter between [A-Z]
At least 1 character from [$#@]
Minimum length of transaction password: 6
Maximum length of transaction password: 12
'''

import re

user_input= input('Enter a password')

match = []
match.append(re.search(r'[A-Z]',user_input))
match.append(re.search(r'[a-z]',user_input))
match.append( re.search(r'[0-9]',user_input))
match.append(re.search(r'[$@#]',user_input)) 

if None in match or len(user_input)<6 or len(user_input)>12:
    print('Invalid')
else:
    print('Valid')




      