class Person:
    def get_string(self):
        self.user_input = input('Enter a string?')
    
    def print_string(self):
        print(self.user_input.upper())

p = Person()
p.get_string()
p.print_string()