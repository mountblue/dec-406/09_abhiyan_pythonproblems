tuples = []

while 1:
    
    choice = input('Enter \'q\' to quit and anykey to continue')
    if choice is 'q':

        print('Sorted by name : ',sorted(tuples,key=lambda x:x[0]))
        print('Sorted by age : ',sorted(tuples,key=lambda x:x[1]))
        print('Sorted by height : ',sorted(tuples,key=lambda x:x[2]))
        exit(0)
    name = input('Enter your name')
    age = input('Enter your age')
    height = input('Enter your height')
    user_data = (name,age,height)
    tuples.append(user_data)
    