def generate_dict(number):
    dict = {}
    for i in range(1,number+1):
        dict[i] = i*i
    
    return dict

print(generate_dict(8))
